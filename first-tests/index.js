const m = require('macaroon');
const testUtil = require('macaroon/test/test-utils')

const rootSecret = 'A unique key that is secret and should not be shared'
const location = 'com.ca-cib.clients.datahub' // macaroon to apply to location
const id = '215ee12f28603'// Math.random().toString(16).slice(2) // unique id of the macaroon

const macaroon = m.newMacaroon({
    version: 2,
    rootKey: rootSecret,
    identifier: id,
    location: location
});

macaroon.addFirstPartyCaveat('clientlive.clientID=1234')

// console.log( 'ID:',testUtil.bytesToString(macaroon.identifier), 
//             '\nLOCATION:', macaroon.location.toString(), 
//            '\nSIGNATURE:',  testUtil.bytesToHex(macaroon.signature))
console.log('asJSON:\n', macaroon.exportJSON())

try {
    macaroon.verify(rootSecret, caveat=>{
        console.log('checking', caveat); return null
    })
    console.log('Verification succedded')
} catch (errorVerified) {
    console.log('Verification failed:', errorVerified.message)
}
const dischargeRootKey = 'client-live-secret';
const thirdPartyCaveatId = '12345577';
macaroon.addThirdPartyCaveat(
    dischargeRootKey, thirdPartyCaveatId, 'clientlive.ca.cib');

const dm = m.newMacaroon({
    rootKey: dischargeRootKey,
    identifier: thirdPartyCaveatId,
    location: 'clientlive.ca.cib',
});

dm.bindToRoot(macaroon.signature);

console.log('asJSON:\n', macaroon.exportJSON())

try {
    macaroon.verify(rootSecret, caveat=>{
        console.log('checking', caveat); return null
    }, [dm])
    console.log('Verification succedded')
} catch (errorVerified) {
    console.log('Verification failed:', errorVerified.message)
}

const b64= testUtil.bytesToBase64(macaroon.exportBinary())
console.log('asBase64:\n', b64)
const macArond = m.importMacaroon(b64)
console.log('asJSON:\n', macArond.exportJSON())