const m = require('macaroon');
const testUtil = require('macaroon/test/test-utils')

const location = 'http://third.party.com'
const users = {
    'guenoledc@yahoo.fr' : {name:'Guenole de Cadoudal (Perso)', password:'password', accounts:[123456, 3000101]},
    'guenole.de-cadoudal@ca-cib.com': {name: 'Guenole de Cadoudal (CACIB)', password:'password', accounts:[999999, 3000101]}
}


createdIds = {}
function getNewId(obj) {
    let id = Math.random(0).toString().slice(2)
    createdIds[id] = obj || {}
    return id
}
function nowPlusMinutes(min) {
    return (new Date(new Date().getTime() + min*60000)).toISOString();
}


module.exports = {
    location: location,
    getNewIdentifier : function(key){
        let uid = getNewId({key:key}) // remember the key provided by the requesting server
        return uid
    },
    getMacaroon : function(uid, account, credentials) {
        // check the caller has an id that is provided from an earlier call to getNewIdentifier
        if(!uid || !uid in createdIds || createdIds[uid]=={} ) return null
        
        // other type of credentials can be implemented
        const email = credentials.user
        const password = credentials.password

        // check that email exists has provided password and has access to the account provided
        console.log('Check email ', email)
        if(!email || !email in users) return null // user does not exists
        console.log('Check password ')
        if(users[email].password!=password) return null // invalid password
        console.log('Check account ', account, users[email].accounts)
        if(users[email].accounts.findIndex(a=>a==account)==-1) return null // account not found for that user

        // let's create the macaroon, using the pre generated key that that someone has requested
        let mac = m.newMacaroon({
            version:2,
            identifier: uid,
            rootKey: createdIds[uid].key, // could be exchanged with the client if encrypted by the thirdParty
            location: location
        })
        mac.addFirstPartyCaveat('account = '+account) // this is a credential for this account
        mac.addFirstPartyCaveat('action in (read,write)') // this is a credential for this account
        // could add here a limitation to reading the account or something else
        mac.addFirstPartyCaveat('time < '+nowPlusMinutes(1))
        return Buffer.from(mac.exportBinary()).toString('base64')
    },
    inspect: function(){
        console.log('### ThirdParty ###')
        console.log('users:\n', users)
        console.log('ids:\n', createdIds)
    }
}