const m = require('macaroon');
const testUtil = require('macaroon/test/test-utils')

const userAuth = require('./userAuthService')

// THIS IS THE RESOURCE SERVER

const superSecret = 'a random secret data'
const location= 'http://main.domain.com'

// IT HANDLES ACCOUNTS AS RESOURCES THAT BELONG TO IDENTIFIED YET ANONYMOUS USER (FOR INSTANCE A PRIVATE BANKING SYSTEM)
const accounts = {
    123456 : {balance: Math.round(Math.random()*100000)/100},
    999999 : {balance: Math.round(Math.random()*100000)/100},
    3000101: {balance: Math.round(Math.random()*100000)/100}
}

function buildExpectedClaims(account) {
    if(!account || !(account in accounts)) return ['not a valid account']
    return [
        ['account','=',account],
        ['action','in', '(read,write)']
    ]
}

let createdIds = {}
function getNewId() {
    let id = Math.random(0).toString().slice(2)
    createdIds[id] = {}
    return id
}
function nowPlusMinutes(min) {
    return (new Date(new Date().getTime() + min*60000)).toISOString();
}

// called for each caveat in the main macaroon, including caveat from the third party macaroons
// All caveats must be valid against the request, and contradictiory caveat must make it fails (eg account = 1 and account = 2 at the same time)
// Basic implementation.
function checkCaveat(claims, caveat) {
    parts = caveat.split(' ')
    console.log('check caveat vs claims', parts, 'VS', claims)
    let i = claims.findIndex(c=>c[0]==parts[0])
    if(i>=0 ) 
        if(claims[i][2]!=parts[2]) return 'invalid claim in request'; else ;
    else if(caveat.startsWith('time < '))
        return new Date().getTime() < Date.parse(caveat.split(' < ')[1]) ? null : 'expired authorisation'
    else return 'missing claim in request'

    
    return null // when ok
}

module.exports = {
    location: location,
    // COLLECT THE ACCESS RIGHT TO THE ACCOUNT, NO NEED TO PROOVE ANYTHING. IT DOES NOT GIVE ACCESS YET
    getAccessAuthorisation : function(accesses) {
        if(!accesses || !accesses.account ) return null

        // initiate the macaroon
        let mac = m.newMacaroon({
            version:2,
            identifier: getNewId(),
            rootKey: superSecret,
            location: location
        })
        
        // for debugging -- Should not be needed normally
        accesses = accesses || {}
        createdIds[Buffer.from(mac.identifier).toString()] = accesses

        // Add the requested predicates to the macaroons, naive implementation
        Object.keys(accesses).forEach(key=>mac.addFirstPartyCaveat(key+' = '+accesses[key].toString()))
        // Add a time limit to the macaroon
        mac.addFirstPartyCaveat('time < '+nowPlusMinutes(10))

        // Add a request for identification proof of the user that it has rights on the account
        let key = Math.random(0).toString().slice(2) // creating a unique key to communicate with the server
        let authId = userAuth.getNewIdentifier(key)
        mac.addThirdPartyCaveat(key, authId, userAuth.location)
        
        mac = Buffer.from(mac.exportBinary()).toString('base64')

        // we now have a serialized macaroon that limits access to a specific account (that may exists or not)
        // and that request a third party system to provide a proof that the user is known to hit and has access to our account

        return mac
    },

    // TRY ACCESSING AN ACCOUNT WITH SOME MACAROONS
    actionOnResource : function(account, macaroonSerialized, thirdPartyMacaroons) {
        thirdPartyMacaroons = thirdPartyMacaroons || []
        let mac = m.importMacaroon(macaroonSerialized)
        let dms = thirdPartyMacaroons.map(s=>m.importMacaroon(s))
        
        //console.log('main:', mac.exportJSON())
        //console.log('dm:', dms[0].exportJSON())

        let claims = buildExpectedClaims(account)
        try {
            mac.verify(superSecret, c=>checkCaveat(claims,c), dms)
            //if(claims.filter(e=>e).length>0) throw new Error('Not all conditions verified')
        } catch (errorVerifying) {
            console.error('Fail verifying:', errorVerifying.message)
            return null
        }
        
        return {account: account, data: accounts[account]}
    },
    inspect: function(){
        console.log('### MainServer ###')
        console.log('accounts:\n', accounts)
        console.log('ids:\n', createdIds)
    }
}