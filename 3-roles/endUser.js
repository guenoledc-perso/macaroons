
const m = require('macaroon');
const privateBank = require('./privateBankService')
const userAuth = require('./userAuthService')

// This is the calling client, using macaroon without understanding them (but still need the library to decode and find the third party)

/////////////////////////////////////
// AUTHENTICATE ON SEVERAL PARTIES
/////////////////////////////////////


// get a serialized macaroon
//const accountNumber = 123456
//const accountNumber = 999999
const accountNumber = 3000101
//const userEmail = 'guenoledc@yahoo.fr'
const userEmail = 'guenole.de-cadoudal@ca-cib.com'
const password = 'password'

// Let's request access to the user's account without revealing who I am
let mac = privateBank.getAccessAuthorisation({account: accountNumber})
if(!mac) {
    console.error('Invalid request')
    process.exit(1)
} else console.log('Autorisation token:\n', mac)

privateBank.inspect()
userAuth.inspect()



/////////////////////////////////////
// LATER, TRY ACCESSING THE ACCOUNT
/////////////////////////////////////

// deserialise the macaroon and request the discharge macaroon from third parties. 
// This can eventually be done on the main server if it has access to the third party
let keep = mac
mac = m.importMacaroon(mac)
let dms = []
mac.caveats.filter(c=>c.location!==undefined).forEach(c=>{ // only those with third party location
    // collect discharged macaroon from the thirdparties
    if(c.location != userAuth.location) return // only those I know of
    let myCredentials = {user: userEmail, password: password}
    let dm = userAuth.getMacaroon(Buffer.from(c.identifier).toString(), accountNumber, myCredentials) // known to be valid only for 60 sec
    if(dm) dm = m.importMacaroon(dm)
    // let's link that retrieved macaroon to the main macaroon to prevent anyone else to use it for any other purpose
    if(dm) dm.bindToRoot(mac.signature)
    if(dm) dms.push(Buffer.from(dm.exportBinary()).toString('base64'))
})

dms.forEach(mac=>console.log('Discharged macaroon:\n', mac))

const waitFor = 1
console.log('Wait '+waitFor+'sec to test')
setTimeout(()=>{
    // try collecting the account object with the macaroons
    let res = privateBank.actionOnResource(accountNumber, keep, dms)
    console.log('Retrieved ressource:\n', res)
}, waitFor*1000)

