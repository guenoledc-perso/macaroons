# Modeling a decentralized authentication service (3-roles folder)

The use case is a private bank that hold accounts for its client but client identities must remain confidentials.

* A single safeguarded central service holds the user identity and what accounts that have access to (`userAuthService.js`). No one but this service should be able to know who is the owner of a particular account
* A bank account service handles the account balance (`privateBankService.js`) that should not know the identity of the account's owner but still must grant the owner access to its account

To perform this, the end user (`end-user.js`), who knows his email, password and his account number, request access to his account without revealing who he is to the private bank service. He is responded with a token that embed a request to prove that he has access to the account in question. Receiving this token, the user, request the identity service to give this proof linked to the token. With this proof (a token that does not contains the identity) he provides both the initial token and the proof token to the private bank service. The service can control all these elements and return the account details

# What are macaroons in short - some theory
more reading at http://theory.stanford.edu/~ataly/Papers/macaroons.pdf

A macaroon is a data structure that starts with the following structure
```
- Header : {location, identifier}
- Signature = hash("secret key", header.identifier)
```
The "secret key" is the `initKey` of the macaroon.

Then the macaroon can be added what is called a caveat : {location, identifier, verifKey}

After adding a caveat `C` into a macaroon `M`, the signature is recalculated as `M.signature = hash(M.signature, concat(C.identifier, C.verifKey))`. Previous signature is lost.
And the macaron structure becomes 

```
- Header : {location, identifier}
- Caveats : [{location, identifier, verifKey}]
- Signature = hash( hash(initKey, header.identifier), concat(C.identifier, C.verifKey)
```
An so on with a new caveats:
```
- Header : {location, identifier}
- Caveats : [{location, identifier, verifKey}, {location, identifier, verifKey} ...]
- Signature = hash( ... hash( hash(initKey, header.identifier), concat(C.identifier, C.verifKey) ...)
```

Outcome:
- Adding a caveat and recalculating the signature does not require the initial secret key
- Modifying the data changes the signature, that cannot be recalculated without the secret key
- Overall cheap to calculate and to serialize

Fields use:
- location : a free form data representing the location of the macaroon or caveat issuer/handler. Typically a url
- identifier : a free form data that identify macaroon or caveat for its issuer/handler, or carries functional data (see after)
- verifKey : 0 for basic caveat and a reference to the initKey of a 3rd party macaroon (see below)

**Basic caveat** (or FirstParty caveat) is interpretable functional condition that restric the scope of what the macaroon is giving access to. For instance, `"account = 123456"` or `"time < 2019-07-14"`. All must be true at the same time for a macaroon to be valid.

Its `location` is null, as assumed to be the same as the macaroon they belong to, its `identifier` stores the condition and its `verifKey` is null.

**3rd party caveat** is a caveat interpreted as a request to provide a third party macaroon that must be initiated with a initKey that only the 3rd party (and the caveat issuer) can deduced from the caveat identifier, and no one else. 

Its `location` is referencing the 3rd party location (a url), its `identifier` maps to the initKey and its `verifKey` is `Encrypt(M.signature, initKey)` used to later verify the 3rd party has the initKey.

**3rd party macaroon** is a normal macaroon that is initialized with an initKey that has been provided by the macaroon's issuer that requested intervention of the third party. It can contains any basic or 3rd party caveats. 

**Verifying a request is valid against a macaroon** : implies for the macaroon issuer to controlling that 
1. the macaroon is correcly formed (ie with the right secret initKey) 
2. all requested 3rd party macaroon are correclty formed (ie created with the shared initKey)
3. all basic caveats (conditions) are respected in the request.

**Requesting data with a macaroon** : is a form of pre authorised request, where the caveats represent the request conditions. Hence a macaroon can be constructed with particular condition and passed to anyone that can at some point use it to get the related data without being further authenticated. The macaroon issuer will verify the macaroon (as above) but will use the condition to create the request insted of controlling the request.

# Using macaroons for the private bank use case 

1. User (or anyone else) requests the private bank to issue an authorisation macaroon for a specific account (123456)
2. Private bank, construct a new macaroon with the following condition
    - account = 123456
    - time < now + 10 minutes, to limit macaroon validity
    - user must prove with the userAuth service that he is identified and has access to the account
3. User sees the request to authenticate and send the 3rd party caveat identifier to the userAuth service, together with the account number and his credentials
4. userAuth service checks the identifier can resolve to the initKey, that the user credentials are valid and that the user has access to account 123456. Then it generates a macaroon with the following conditions
    - account = 123456
    - action in (read,write)
    - time < now + 1 minute, further time limiting
5. User with both macaroons, request the private bank service to read the balance of the account. It verifies the validity of the macaroon, and checks that requests conditions are valid against all macaroons restrictions (both the main one and the 3rd party one) and that time based conditions are respected. If successfull the account balance is returned

# Alternative implementation with JWT

1. User can authenticate with a userAuth service that can deliver a JWT token containing the list of accounts he has access to (or just the one requested) but does not contains the user id. It also includes a validity date. This JWT is signed by the private key of the userAuth service
2. User passes the JWT to the private bank service while requesting the account info
3. Private bank service checks the JWT with the userAuth public key (or talking to it directly) and if successfull and JWT contains proof of access to the requested account, it can return the account balance

So this works well also in this simple use case, and seems more simple to implement. So why should we bother with macaroon.

# When JWT will not be enough 

In a world where responsibilities over systems are distributed, if the userAuth service is controlled by a team and that service gives a JWT with the account and all the possible access rights (read and write) for the user who requested the JWT, it is not possible to pass a JWT that only allow reading the account as the JWT is not modifiable.
It would imply asking the userAuth team to create a new version of the authentication service --> delay.

Say the scenario is the following:
- User wants to buy an expensive product and buyer wants to be sure he has the money on his account
- User requests macaroon authorisation to his account, and the proof of accessing (read & write) his account
- User reduces the macaroon by adding "action = read" on top of "action in (read,write)" and pass both macaroons to the buyer
- Buyer requests himself the private bank service (he has less than few minutes to do it) passing the macaroons
- Private bank service recognize that the request is legitimate as the request only need reading and no writing and return the balance (if all is done in the right timing)

This will be more complicated with JWT, as it implies changing software where it shouldn't.

# Reflexion for DSP2 regulation
The DSP2 regulation forces banks to allow their client delegating their rights to 3rd parties for accessing/managing their account. Using macaroon in this context would provide lot's of flexibility .... to be further studied.

# Install this demo

This inplementation uses the `npm i macaroon` package which is a cross language implementation. 
But it contains a bug (https://github.com/go-macaroon/js-macaroon/pull/40) still in pull request.
so I choose to refer to the corrected repo https://github.com/guggero/js-macaroon in the package.json

Now just do 
```sh
git clone https://gitlab.com/guenoledc-perso/macaroons.git
cd test-macaroons
npm i 
node 3-roles/endUser.js 
```